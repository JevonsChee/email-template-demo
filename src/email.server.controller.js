'use strict';

/**
 * Module dependencies.
 */
var path = require('path'),
  config = require(path.resolve('./config/config')),
  tokenHelper = require(path.resolve('./modules/users/server/helpers/token.server.helper')),
  workerController = require(path.resolve('./modules/checkins/server/controllers/worker.server.controller')),
  scheduleCheckin = require(path.resolve('./modules/checkins/server/helper/schedule-checkin.server.helper')),
  async = require('async'),
  moment = require('moment'),
  chalk = require('chalk'),
  _ = require('lodash');

function prepareEmailPayload (emailTemplateName, mailOptions, params) {
  return {
    templatePath: emailTemplateName,
    options: mailOptions,
    params: params
  };
}

exports.sendCheckinReminderEmail = function (transaction, templateName) {
  return new Promise(function(fulfill, reject) {
    var users = transaction.users;
    var links = transaction.links;
    var schedule = transaction.schedule;

    var monthNames = moment.months();

    var date = new Date();
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var fullDate = monthNames[monthIndex] + ' ' + day;

    var count = 0;
    async.whilst(
      function() { return count < users.length; },
      function(callback) {
        var user = users[count];
        var link = _.find(links, function(link) {
          return link.userId.equals(user._id);
        });
        var mailOptions = {
          to: user.email,
          subject: 'Check-ins for today',
          replyTo: config.mailer.support
        };
        var params = {
          name: user.name,
          url: link.url,
          appName: config.app.title,
          supportEmail: config.mailer.support,
          todayDate: fullDate
        };
        count++;
        var emailPayload = prepareEmailPayload(templateName, mailOptions, params);
        workerController.enqueueEmail(emailPayload, callback);
      },
      function(err, n) {
        if (err) {
          reject(err);
          return;
        }

        schedule.hasSentReminder = true;
        schedule.save().then(function(schedule) {
          fulfill(transaction);
        }).catch(function(err) {
          reject(err);
        });
      }
    );
  });
};

exports.sendSignupVerificationEmail = function (transaction, templateName) {
  return new Promise(function(fulfill, reject) {
    var toEmail = transaction.toEmail;
    var link = transaction.link;

    var mailOptions = {
      to: toEmail,
      subject: 'Verification link for ' + config.app.title,
      replyTo: config.mailer.support
    };
    var params = {
      url: link,
      appName: config.app.title
    };

    var emailPayload = prepareEmailPayload(templateName, mailOptions, params);
    workerController.enqueueEmail(emailPayload, function(err) {
      if (err) {
        reject(err);
        return;
      }

      fulfill(transaction);
    });
  });
};

exports.sendCheckinSummaryEmail = function (transaction, templateName) {
  return new Promise(function(fulfill, reject) {

    var schedule = transaction.schedule;
    var accomplishmentQACheckins = transaction.accomplishmentsOnly;
    var listOfAccomplishments = _.map(accomplishmentQACheckins, function(obj) {
      return {
        name: obj.user.name,
        email: obj.user.email,
        username: obj.user.username,
        answer: obj.answer,
        url: obj.link.url,
        avatarURL: obj.user.avatarURL || 'http://www.nocaptionneeded.com/wp-content/uploads/2015/02/Grey-blank-panel.png'
      };
    });

    console.log(listOfAccomplishments)

    var count = 0;
    async.whilst(
      function() { return count < listOfAccomplishments.length; },
      function(callback) {
        var userInfo = listOfAccomplishments[count];
        var mailOptions = {
          to: userInfo.email,
          subject: 'Check-ins Summary',
          replyTo: config.mailer.support
        };
        var params = {
          name: userInfo.name,
          url: transaction.link, // 'dummy link',
          appName: config.app.title,
          checkinURL: userInfo.url,
          avatarURL: userInfo.avatarUrl,
          checkinAccomplishments: listOfAccomplishments
        };
        console.log('Param : ' + params)
        count++;
        var emailPayload = prepareEmailPayload(templateName, mailOptions, params);
        workerController.enqueueEmail(emailPayload, callback);
      },
      function(err, n) {
        if (err) {
          reject(err);
          return;
        }

        schedule.hasSentSummary = true;
        schedule.save().then(function(schedule) {
          fulfill(transaction);
        }).catch(function(err) {
          reject(err);
        });
      }
    );
  });
};

exports.sendInviteMembersEmail = function (transaction, templateName) {
  return new Promise(function(fulfill, reject) {
    var links = transaction.links;
    var invitationList = transaction.invitationList;
    var user = transaction.user;
    var signup = transaction.signup;
    var count = 0;
    async.whilst(
      function() { return count < links.length; },
      function(callback) {
        var mailOptions = {
          to: invitationList[count].email,
          subject: user.name + ' has invited you to join ' + config.app.title,
          replyTo: config.mailer.support
        };
        var params = {
          name: user.name,
          appName: config.app.title,
          url: links[count]
        };
        count++;
        var emailPayload = prepareEmailPayload(templateName, mailOptions, params);
        workerController.enqueueEmail(emailPayload, callback);
      },
      function (err, n) {
        if (err) {
          reject(err);
          return;
        }
        fulfill(transaction);
      }
    );
  });
};

exports.sendWelcomeOnboardedEmail = function (transaction, templateName) {
  return new Promise(function(fulfill, reject) {
    var user = transaction.user;
    var teamSetting = transaction.teamSetting;
    // teamSetting.config.
    var testWhenIsNextReminderJob = scheduleCheckin.prepareCronJob('send checkin reminder', user.teamId, teamSetting.config);
    var nextReminderRunAt = testWhenIsNextReminderJob.attrs.nextRunAt;
    var nextRunMoment = moment.tz(nextReminderRunAt, _.first(teamSetting.config.timezone.utc));

    var count = 0;
    var mailOptions = {
      to: user.email,
      subject: 'Welcome to ' + config.app.title,
      replyTo: config.mailer.support
    };
    var params = {
      name: user.name,
      appName: config.app.title,
      url: tokenHelper.assembleTeamBaseUrl(user.teamId) + '/settings/team/members',
      team: user.teamId,
      nextCheckIn: nextRunMoment.format('dddd, MMMM D [at] h:mma'),
      supportEmail: config.mailer.support
    };

    var emailPayload = prepareEmailPayload(templateName, mailOptions, params);
    workerController.enqueueEmail(emailPayload, function(err) {
      if (err) {
        reject(err);
        return;
      }

      fulfill(transaction);
    });
  });
};

exports.sendResetPasswordConfirmEmail = function (transaction, templateName) {
  return new Promise(function(fulfill, reject) {
    var user = transaction.user;
    var count = 0;
    var mailOptions = {
      to: user.email,
      subject: 'Your password has been changed',
      replyTo: config.mailer.support
    };
    var params = {
      name: user.name,
      appName: config.app.title,
      supportEmail: config.mailer.support
    };

    var emailPayload = prepareEmailPayload(templateName, mailOptions, params);
    workerController.enqueueEmail(emailPayload, function(err) {
      if (err) {
        reject(err);
        return;
      }

      fulfill(transaction);
    });
  });
};

exports.sendResetPasswordEmail = function (transaction, templateName) {
  return new Promise(function(fulfill, reject) {
    var user = transaction.user;
    var link = transaction.link;
    var count = 0;
    var mailOptions = {
      to: user.email,
      subject: 'Password Reset',
      replyTo: config.mailer.support
    };
    var params = {
      name: user.name,
      appName: config.app.title,
      url: link,
      supportEmail: config.mailer.support
    };

    var emailPayload = prepareEmailPayload(templateName, mailOptions, params);
    workerController.enqueueEmail(emailPayload, function(err) {
      if (err) {
        reject(err);
        return;
      }

      fulfill(transaction);
    });
  });
};
